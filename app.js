//import express
const express = require('express');
const cors = require("cors");
const app = express()
const port = 5000;
app.use(cors());
app.listen(port,()=>{
    console.log(`Server is running on port: ${port}`)
})
const { default: mongoose } = require('mongoose');

//import mangoose
const mangoose = require('mongoose');
//connect mangoose
mangoose.connect("mongodb+srv://lynn:0000@cluster0.nrquz.mongodb.net/?retryWrites=true&w=majority");
//check if database is connected or nnot
const db = mangoose.connection;
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
    console.log('connected')
})

//require the router files
//link the order with it's page
// const adminRouter=require('./routes/admin');
// const userRouter=require('./routes/users');
// app.use('/admin',adminRouter)
// app.use("/users",userRouter)
const RecipeRouter=require('./my-app/backend/routes/recipes');
app.use("/recipes",RecipeRouter)

const IngredientRouter=require('./my-app/backend/routes/Ingredient');
app.use("/ingredients",IngredientRouter)

const CategoryRouter=require('./my-app/backend/routes/categories');
app.use("/categories",CategoryRouter)

const aboutUsRouter=require('./my-app/backend/routes/aboutus');
app.use("/aboutus",aboutUsRouter)